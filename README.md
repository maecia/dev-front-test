# Test Front-End Inside
## Introduction

- L'objectif est de créer une liste de bière provenant de https://punkapi.com/documentation/v2
- Vous devez utiliser le framework Javascript Vue.js.
- Seront évalués :
    - L'organisation et la propreté du code
    - Utilisation des standards ES6 +
    - L'organisation des css
    - Le fonctionnel
    - La créativité

## Consignes

### Étape 1
- Récupérez et affichez la liste des 20 premières bières
- Deux affichages doivent être possibles : liste et grid
- Ajouter une pagination pour accéder aux bières suivantes
- Gérer le responsive afin que le site soit présentable sur mobile.

### Étape 2
- Ajouter un champ de recherche
- Au clic sur une bière, ouvrir une page avec les informations de celle-ci.

### Étape 3
- Ajouter la possibilité d'ajouter vos bières en favoris
- Ajouter un filtre pour ne retrouver que vos bières favorites

### Bonus
- Vous êtes libres d'ajouter des fonctionnalités qui vous semblent pertinentes.

## Rendu
Le rendu de l'exercice doit se faire :

- sur un repo privé github/bitbucket/gitlab/etc avec un accès pour technique@maecia.com
- ou en envoyant l'archive zipée par mail à technique@maecia.com

